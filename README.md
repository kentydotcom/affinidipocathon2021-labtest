# Affinidi POCathon Health Insurance Use Case Implementation - Lab-Test - Issuer

## Introduction

Welcome to the Lab-Test Portal for the Health Insurance Use Case Implementation designed by Dr Kent Lau [https://linkedin.com/in/kentglau].

<br/>
<br/>
<img src="./src/assets/images/icons/lab-test-portal.png">